import 'package:flutter/material.dart';
import 'Spesifik_Mobil.dart';

void _showSimpleDialog(context) {
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Permintaan Terkirim Hubungi No :081239872070 Untuk pengambilan Mobil",
                    style: TextStyle(fontSize: 20.0),

                  ),
                ),

                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          ),
        ],
      );
    },
  );
}

class DetailMobil extends StatelessWidget {
  final String title;
  final double price;
  final String color;
  final String gearbox;
  final String fuel;
  final String brand;
  final String path;

  DetailMobil(
      {this.title,
        this.price,
        this.color,
        this.gearbox,
        this.fuel,
        this.brand,
        this.path});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreenAccent,
        title: new Text(' RENCAR MOBIL',style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 40),),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Text(title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30,
          ),
          ),
          Text(
            brand,
            style: TextStyle(fontSize: 15,
            ),
          ),
          Hero(tag: title, child: Image.asset(path)),

          Row(

            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SpesifikMobil(
                name: '1 HARI',
                price: price * 1,
                name2: 'RUPIAH',
              ),
              SpesifikMobil(
                name: '3 HARI',
                price: price * 3,
                name2: 'RUPIAH',
              ),
              SpesifikMobil(
                name: '7 HARI',
                price: price * 7,
                name2: 'RUPIAH',
              )
            ],
          ),
          SizedBox(height: 30),
          Text(
            'SPECIFICATIONS',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SpesifikMobil(
                name: 'WARNA',
                name2: color,
              ),
              SpesifikMobil(
                name: 'TRANSMISI',
                name2: gearbox,
              ),
              SpesifikMobil(
                name: 'FUEL',
                name2: fuel,
              )
            ],
          ),
          SizedBox(height: 30),

          RaisedButton(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            onPressed: (){
              _showSimpleDialog(context);
            },
            padding: EdgeInsets.all(10.0),
            color: Colors.amberAccent,
            child: Text(
              'Rental',
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}