import 'package:flutter/material.dart';

class CarItem {
  final String title;
  final double price;
  final String path;
  final String color;
  final String gearbox;
  final String fuel;
  final String brand;

  CarItem(
      {@required this.title,
        @required this.price,
        @required this.path,
        @required this.color,
        @required this.gearbox,
        @required this.fuel,
        @required this.brand});
}

CarsList allCars = CarsList(cars: [
  CarItem(
      title: 'Lamborghini Aventador',
      price: 2000000,
      color: 'HITAM',
      gearbox: '6',
      fuel: '10L',
      brand: 'lamborghini',
      path: 'images/assets/car1.jpg'),
  CarItem(
      title: 'Ferrari 488 Pista',
      price: 1500000,
      color: 'MERAH',
      gearbox: '6',
      fuel: '10L',
      brand: 'Ferrari',
      path: 'images/assets/car2.jpg'),
  CarItem(
      title: 'Jaguar F-Type',
      price: 1300000,
      color: 'silver',
      gearbox: '6',
      fuel: '12L',
      brand: 'Jaguar',
      path: 'images/assets/car3.jpg'),
  CarItem(
      title: 'Audi PB18 E-Tron',
      price: 1200000,
      color: 'silver',
      gearbox: '6',
      fuel: '9',
      brand: 'Audi',
      path: 'images/assets/car4.jpg'),
  CarItem(
      title: 'BMW I8 Concept',
      price: 1100000,
      color: 'White',
      gearbox: '6',
      fuel: '11L',
      brand: 'BMW',
      path: 'images/assets/car5.jpg'),
  CarItem(
      title: 'Honda Sport EV concept',
      price: 1000000,
      color: 'Silver',
      gearbox: '6',
      fuel: '15',
      brand: 'Honda',
      path: 'images/assets/car6.jpg'),
]);

class CarsList {
  List<CarItem> cars;

  CarsList({this.cars});
}