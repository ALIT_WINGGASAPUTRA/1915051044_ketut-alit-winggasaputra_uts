import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app15/Mobil.dart';


class Tampilanmobil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreenAccent,
        title: new Text('RENCAR MOBIL',style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 40),),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
          ),
          Text('MOBIL YANG TERSEDIA',
            style: TextStyle(color: Colors.lightBlueAccent, fontWeight: FontWeight.bold, fontSize: 30),),


          Padding(
            padding: const EdgeInsets.all(15.0),
            child: MobilTs(),
          ),
        ],
      ),
    );
  }
}
