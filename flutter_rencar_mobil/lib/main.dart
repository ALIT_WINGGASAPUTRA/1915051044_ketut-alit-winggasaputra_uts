import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'MyProfil.dart';
import 'Tampilan_mobil.dart';

void main() {
  runApp(
    new MaterialApp(
      home: new Home(),
      routes: <String, WidgetBuilder>{
        '/page1' : (BuildContext context) => new Home(),
        '/page2' : (BuildContext context) => new MyProfil(),
        '/page3' : (BuildContext context) => new Tampilanmobil(),
      },
    ),
  );
}

class  Home  extends  StatefulWidget  {
  @override
  _HomeState  createState()  =>  new  _HomeState();
}

class _HomeState extends State<Home> {


  String  _jk="";

  void _pilihJk(String value){
    setState(() {

    });
  }

  var _nama = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(
        title: new Text('PEMESANAN RENCAR MOBIL',style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 40),),
        leading:  new  Icon(Icons.menu,color:Colors.black,),
        centerTitle:  true,
        backgroundColor:Colors.yellowAccent,shadowColor:Colors.lightBlue,
        actions:  <Widget>[
          new  IconButton(
            icon:  Icon(Icons.person_outline,color:Colors.black),
            onPressed: (){
              Navigator.pushNamed(context, '/page2');
            },
          )
        ],
      ),

      body:  ListView(

        children:  <Widget>[

          new  Container(
            color: Color(0xa6eee0aa),
            padding:  new  EdgeInsets.all(50.0),
            child:  new  Column(
              children:  <Widget>[
                new Container(
                  child: Image.asset('images/logo.jpg',height: 300,width: 300,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                new TextField(
                  controller:  _nama,
                  decoration: new InputDecoration(
                      hintText:  "Masukkan  Nama  Lengkap",
                      labelText:  "NAMA LENGKAP",
                      border:  new  OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(7.0)
                      )
                  ),
                ),

                new  Padding(padding:  new  EdgeInsets.only(top:  20.0)),

                new TextField(
                  keyboardType:  TextInputType.number,
                  maxLength:  13,
                  decoration: new InputDecoration(
                      hintText:  "No Telpon",
                      labelText:  "Masukan No Telpon",
                      border:  new  OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)
                      )
                  ),
                ),
                new  Padding(padding:  new  EdgeInsets.only(top:  10.0)),

                new  RadioListTile(
                  value:  "Laki-laki",
                  groupValue: _jk,
                  title:  new  Text("Laki-laki"),
                  onChanged: (String value){
                    _pilihJk(value);
                  },
                  subtitle:  new  Text("Pilih  ini  jika  Anda  Laki-laki"),
                ),

                new  Padding(padding:  new  EdgeInsets.only(top:  0.0)),

                new  RadioListTile(
                  value:  "Perempuan",
                  groupValue: _jk,
                  title:  new  Text("Perempuan"),
                  onChanged: (String value){
                    _pilihJk(value);
                  },
                  subtitle:  new  Text("Pilih  ini  jika  Anda  Perempuan"),
                ),

                new  Padding(padding:  new  EdgeInsets.only(top:  20.0)),

                new  Row(
                  children: <Widget>[
                    Expanded(
                      child:  TextField(

                        decoration:  InputDecoration(
                            labelText:  "Tanggal Memesan",
                            suffix:  Text('dd/mm/yyyy'),
                            border:  new  OutlineInputBorder(
                                borderRadius:  new  BorderRadius.circular(10.0)
                            ),
                            //filled: true,
                            hintText:  'masukan tanggal Memesan'),
                      ),
                    ),
                    SizedBox(
                      width:  10,
                    ),
                    Expanded(
                      child:  TextField(

                        decoration:  InputDecoration(
                            labelText:  "Tanggal Mengembalikan",
                            suffix:  Text('dd/mm/yyyy'),
                            border:  new  OutlineInputBorder(
                                borderRadius:  new  BorderRadius.circular(10.0)
                            ),
                            //filled: true,
                            hintText:  'Masukan Tanggal Mengembalikan'),

                      ),
                    ),
                  ],
                ),

                new  Padding(padding:  new  EdgeInsets.only(top:  20.0)),

                Container(
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 20),
                  child: RaisedButton(
                    onPressed: (){
                      var route = new MaterialPageRoute(builder: (context)=> Tampilanmobil(),
                      );
                      Navigator.of(context).push(route);
                    },
                    padding: EdgeInsets.all(10.0),
                    color: Colors.amberAccent,
                    textColor: Colors.black,
                    child: Text(
                      'Search',
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
